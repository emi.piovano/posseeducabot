const express = require('express');
const bodyParser = require('body-parser');
const { Client, LocalAuth, Buttons, List, MessageMedia } = require('whatsapp-web.js');
const qrcodeTerminal = require('qrcode-terminal');
const { Pool } = require('pg');



// Configurar la conexión a la base de datos PostgreSQL
const pool = new Pool({
  user: 'postgres',
  host: 'localhost',
  database: 'PosseEducaBot',
  password: 'Poster',
  port: 5432,
});

// const pool = new Pool({
//   connectionString: 'postgres://posseeducabot_user:5PtDwnChrHknJRWD57zHwDjDC9bYJKYW@dpg-ck7i2sfsasqs73fqssh0-a/posseeducabot',
//   ssl: {
//     rejectUnauthorized: false // Esto es necesario si el servidor tiene un certificado autofirmado
//   }
// });
// const pool = new Pool({
//   user: 'user',
//   host: 'db',
//   database: 'posseeducabot',
//   password: 'Poster',
//   port: 5432,
// });

// Evento de conexión
pool.on('connect', () => {
  console.log('Conectado a la base de datos');
});

// Evento de error
pool.on('error', (err, client) => {
  console.error('Error inesperado en la conexión a la base de datos', err);
  process.exit(-1);
});


const createTableQuery = `
  CREATE TABLE IF NOT EXISTS whatsapp_sessions (
    id SERIAL PRIMARY KEY,
    session JSONB NOT NULL
  );
`;

pool.query(createTableQuery, (err, res) => {
  if (err) {
    console.error('Error creando la tabla whatsapp_sessions:', err.stack);
  } else {
    console.log('Tabla whatsapp_sessions creada o ya existente');
  }
});

const app = express();
const PORT = 3000;
const authStrategy = new LocalAuth();

const client = new Client({
  authStrategy: authStrategy,
});

// Intenta recuperar la sesión de la base de datos
pool.query('SELECT session FROM whatsapp_sessions WHERE id = 1', (err, res) => {
  if (err) throw err;
  if (res.rows.length) {
    authStrategy.loadSession(JSON.parse(res.rows[0].session));
  }
});


// Middlewares
app.use(bodyParser.json());

client.on('qr', (qr) => {
  qrcodeTerminal.generate(qr, { small: true });
});

client.on('authenticated', (session) => {
  if (session) {
    // Guarda la sesión en la base de datos
    pool.query(
      'INSERT INTO whatsapp_sessions (id, session) VALUES (1, $1) ON CONFLICT (id) DO UPDATE SET session = $1',
      [JSON.stringify(session)],
      (err) => {
        if (err) throw err;
      }
    );
  } else {
    console.error('La sesión es nula, no se puede guardar.');
  }
});

client.on('ready', () => {
  console.log('Cliente de WhatsApp listo!');
});

client.initialize();

app.post('/SendMessage', async (req, res) => {
  try {
    const { phoneNumber, messageText } = req.body;
    console.log(phoneNumber)

    let chatId = `${phoneNumber}@c.us`; // WhatsApp utiliza el formato {phoneNumber}@c.us para los chat IDs
    await client.sendMessage(chatId, messageText);

    res.json({ success: true, message: 'Mensaje enviado correctamente' });
  } catch (error) {
    console.error(`Error enviando mensaje: ${error}`);
    res.status(500).json({ success: false, message: 'Error enviando mensaje' });
  }
});

app.post('/AddToGroup', async (req, res) => {
  try {
    const { groupName, participantId } = req.body;
    console.log(req.body);
    // Busca el grupo por su nombre
    let group = null;
    const chats = await client.getChats();
    for (let chat of chats) {
      if (chat.isGroup && chat.name === groupName) {
        group = chat;
        break;
      }
    }

    if (group) {
      // Añade al participante al grupo
      await group.addParticipants([`${participantId}@c.us`]);
      res.json({ success: true, message: 'Participante añadido correctamente al grupo' });
    } else {
      res.status(404).json({ success: false, message: 'Grupo no encontrado' });
    }
  } catch (error) {
    console.error(`Error añadiendo participante al grupo: ${error}`);
    res.status(500).json({ success: false, message: 'Error añadiendo participante al grupo' });
  }
});


app.listen(PORT, () => {
  console.log(`Servidor corriendo en http://localhost:${PORT}`);
});
